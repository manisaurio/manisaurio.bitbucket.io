var Validacion = (function(){ 
    return {
        "Documento": undefined,
        "Empleado": undefined
    }
})();

/*
Validacion.Documento = (function(){
    var _nickname = function( _elemento ){

        var error = document.querySelector('.error');

        _elemento.addEventListener("keyup", function (event) {
            // Se crea el validador de la restricción
            var constraint = new RegExp("^[a-zA-ZñÑ]+$");

            // ¡Se hace la revisión!
            if( constraint.test(_elemento.value) ){
                // En caso que haya un mensaje de error visible, si el campo
                // es válido, removemos el mensaje de error.
                error.innerHTML = ""; // Limpia el contenido del mensaje
                error.className = "error"; // Restablece el estado visual del mensaje
            }else{
                // El código postal no cumple la restricción, usamos la API de Validaciones para
                // dar un mensaje sobre el formato requerido para este país
                //_elemento.setCustomValidity("Solo letras");
                // Si el campo no es válido, mostramos un mensaje de error.
    error.innerHTML = "¡Yo esperaba una dirección de correo, cariño!";
    error.className = "error active";
            }

            
            if (_elemento.validity.typeMismatch) {
                _elemento.setCustomValidity("¡Yo esperaba una dirección de correo, cariño!");
            } else {
                _elemento.setCustomValidity("");
            }
            
        }, false);

    };  

    return{
        "_nickname": _nickname
    };
})();
*/
Validacion.Documento = (function(){

    var _numoficio = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 10;
        return _event.target.value.length < MAXLENGTH;
    };

    var _numhojas = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 2;

        var _key = (document.all) ? _event.keyCode : _event.which;
        //var _key = (_event.keyCode ? _event.keyCode : _event.which);
        //var _key = window.event ? window.event.keyCode : _event.which;
        
        var _regexp  = /[0-9]/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _otrotipoinput = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 25;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _numanexos = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 2;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /[0-9]/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _suscriptorinput = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 100;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ. ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _cargo = function( _event ){
        if( _controlkey( _event.keyCode ) ){
            return true;
        };

        var MAXLENGTH = 25;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _controlkey = function( _keycode){
        switch ( _keycode ) {
            case 8:     //backspace
            case 9:     //tab
            case 13:    //enter
            case 16:    //shift
            case 35:    //end
            case 36:    //home
            case 37:    //left arrow
            case 39:    //right arrow
            case 46:    //delete
                return true;
        };
        return false
    };

    return{
        "numoficio": _numoficio,
        "numhojas": _numhojas,
        "otrotipoinput": _otrotipoinput,
        "numanexos": _numanexos,
        "suscriptorinput": _suscriptorinput,
        "cargo": _cargo
    };
})();

Validacion.Empleado = (function(){

    var _init = function(){

    }

    var _nombre = function(){
        var _inputNombre = document.getElementById("nombre");

    }
    
    var _palabra = function( _event ){
        //Validacion.Empleado.nombre
        if( _controlkey( _event.keyCode ) ){
            return true;
        };
       
        var MAXLENGTH = 100;

        var _key = (document.all) ? _event.keyCode : _event.which;
        
        var _regexp  = /^[a-zA-ZñÑ. ]+$/;  //regular expression
        var _keyString = String.fromCharCode( _key );

        var _parent = _event.target.parentNode;
        var _smallMensaje = document.getElementById(_event.target.id + 'Small');
        if( !_regexp.test( _keyString ) ){
            _smallMensaje.textContent = "No se admiten caracteres especiales ni numeros";
            _parent.setAttribute("class", "form-group col-md-12 has-error");
        }else if(_event.target.value.length < 3){
            _smallMensaje.textContent = "Minimo 3 caracteres";
            _parent.setAttribute("class", "form-group col-md-12 has-error");
        }else{
            _smallMensaje.textContent = "";
            _parent.setAttribute("class", "form-group col-md-12");
        }

        return _regexp.test( _keyString ) && ( _event.target.value.length < MAXLENGTH );
    };

    var _controlkey = function( _keycode){
        switch ( _keycode ) {
            case 8:     //backspace
            case 9:     //tab
            case 16:    //shift
            case 27:    //escape
            case 35:    //end
            case 36:    //home
            case 37:    //left arrow
            case 39:    //right arrow
            case 46:    //delete
                return true;
        };
        return false
    };

    return{
        "palabra": _palabra
    };
})();